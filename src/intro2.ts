function getCustomerEmails( customers ) {

	return customers.pluck( 'email' );
}

function getStockTotals( inventory ) {
	return inventory.map(item =>({
			product   : item.productName,
			totalValue: item.quantity * item.price,
	}))

}

function getOutOfStocksProducts( products ) {

	return products.filter(product => !product.quantity);

}

function getEmailsFromDepartment( employees, department ) {

	return employees
		.where('department', department)
		.pluck('email');

}












getCustomerEmails( [] );
getStockTotals( [] );
getOutOfStocksProducts( [] );
getEmailsFromDepartment( [], '' );