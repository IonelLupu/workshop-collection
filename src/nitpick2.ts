import collect from 'collect.js';
import StyleChecker from "./lib/StyleChecker";
import CheckedFile from "./lib/CheckedFile";

class NitPick {
	private checkers : Collection<StyleChecker>;
	/*[
	 'CSSChecker',
	 'HTMLChecker',
	 'JavaScriptChecker',
	 'GoLangChecker',
	 ];*/

	construct(checkers){
		this.checkers = checkers;
	}

	checkFile(file: string) {
		if (this.hasCheckerFor(file)) {
			let checker = this.getCheckerFor(file);
			let violations = checker.check(file);
			return new CheckedFile(file, violations);
		} else {
			return new CheckedFile(file, []);
		}
	}

	hasCheckerFor(file) {
		for (let checker of this.checkers) {
			if (checker.canCheck(file)) {
				return true;
			}
		}
		return false;
	}

	getCheckerFor(file) {
		for (let checker of this.checkers) {
			if (checker.canCheck(file)) {
				return checker;
			}
		}
	}
}
