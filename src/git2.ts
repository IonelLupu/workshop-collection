import activity from './data/activity';
import collect from 'collect.js';

export default (function getUserScore () {

	let events = collect(activity);

	let types = events.pluck('type');

	let scores = {
		"PushEvent"   : 5,
		"CreateEvent" : 4,
		"IssueEvent"  : 3,
		"CommentEvent": 2
	}

	let userScore = types.sum(type => scores[type] || 1);

	console.log( "refactored user score is  ->", userScore );
})();