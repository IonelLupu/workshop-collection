import activity from './data/activity';

export default (function getUserScore() {

	let types = [];

	for ( let result of activity ) {
		types.push( result.type );
	}

	let userScore = 0;

	for ( let type of types ) {
		switch ( type ) {
			case "PushEvent":
				userScore += 5;
				break;
			case "CreateEvent":
				userScore += 4;
				break;
			case "IssueEvent":
				userScore += 3;
				break;
			case "CommentEvent":
				userScore += 2;
				break;
			default:
				userScore += 1;
				break;
		}
	}

	console.log( "user score is  ->", userScore );
})();