function getCustomerEmails( customers ) {
	let customerEmails = [];

	for ( let customer of customers ) {
		customerEmails.push( customer.email );
	}

	return customerEmails;
}

function getStockTotals( inventory ) {
	let stockTotals = [];

	for ( let item of inventory ) {
		stockTotals.push( {
			product   : item.productName,
			totalValue: item.quantity * item.price,
		} );
	}

	return stockTotals;
}

function getOutOfStocksProducts( products ) {
	let outOfStockProducts = [];

	for ( let product of products ) {
		if ( !product.quantity ) {
			outOfStockProducts.push( product );
		}
	}

	return outOfStockProducts;
}

function getEmailsFromDepartment( employees, department ) {
	let emails = [];

	for ( let employee of employees ) {
		if ( employee.department == department ) {
			emails.push( employee.email )
		}
	}

	return emails;
}












getCustomerEmails( [] );
getStockTotals( [] );
getOutOfStocksProducts( [] );
getEmailsFromDepartment( [], '' );