import employees from './data/employees2'

const minSales = 100;
export default (function getTotalRevenueForCountry( country ) {

	let totalRevenue = 0;

	totalRevenue = employees
		.where('country', country)
		.map(employee => employee.customers)
		.flatten(1)
		.map(customer => customer.orders.sum('total') )
		.filter(total => total > minSales).sum();

	console.log( "refactored Total revenue->", totalRevenue );
})( 'Romania' )
