import Book from './lib/Book';
import Order from './lib/Order';
import Coupon from './lib/Coupon';

export default (function (  ) {

	let books = [
		new Book({'price' : 20}),
		new Book({'price' : 75}),
		new Book({'price' : 55}),
	];

	let order = new Order(books);

	let coupon = new Coupon({
		code : 'FV554',
		value : 50,
	});

	order.applyCoupon(coupon);

	console.log("book ->",order.total);
})()