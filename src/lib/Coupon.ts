export default class Coupon{

	public value;
	public code;

	constructor( attributes ) {
		this.code = attributes.code;
		this.value = attributes.value;
	}
}