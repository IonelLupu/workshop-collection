export default class StyleChecker{
	public extension: string;
	public rules: any[];

	constructor(public name = ''){}

	canCheck(file){
		return file.endsWith(this.extension);
	}

	check(file){
		let violations = this.rules.map(rule => rule.check(file));

		return violations.filter(rule => !!rule);
	}
}