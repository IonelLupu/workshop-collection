export default class CheckedFile{

	constructor(public file: string, public violations: string[]){}
}