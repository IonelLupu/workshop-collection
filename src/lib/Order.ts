import Coupon from './Coupon';
import collect from 'collect.js';

export default class Order {
	public coupon;
	public products;
	constructor( products ) {
		this.products = collect(products);
	}

	get total() {
		let discount = 0;
		if(this.coupon){
			discount = this.coupon.value;
		}

		return this.products.sum('price') - discount
	}

	applyCoupon( coupon: Coupon ) {
		this.coupon = coupon;
	}
}