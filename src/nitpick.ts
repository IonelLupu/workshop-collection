import StyleChecker from "./lib/StyleChecker";
import CheckedFile from "./lib/CheckedFile";
import NullChecker from "./lib/NullChecker";

class NitPick {
	private checkers: StyleChecker[];
	/*[
	 'CSSChecker',
	 'HTMLChecker',
	 'JavaScriptChecker',
	 'GoLangChecker',
	 ];*/

	construct ( checkers ) {
		this.checkers = checkers;
	}

	checkFile ( file: string ) {
		let checker = this.getCheckerFor( file );
		let violations = checker.check( file );
		return new CheckedFile( file, violations );
	}

	getCheckerFor ( file ) {
		return this.checkers.first( checker => checker.canCheck( file ) ) || new NullChecker();

	}
}
