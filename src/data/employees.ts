import collect from 'collect.js';

import Book from './../lib/Book';
import Order from './../lib/Order';
import Employee from './../lib/Employee';
import Customer from './../lib/Customer';


let b1 = new Book({'price' : 20});
let b2 = new Book({'price' : 75});
let b3 = new Book({'price' : 12});
let b4 = new Book({'price' : 72});
let b5 = new Book({'price' : 56});
let b6 = new Book({'price' : 12});
let b7 = new Book({'price' : 5});
let b8 = new Book({'price' : 17});

let o1 = new Order([b1,b6,b7]);
let o2 = new Order([b2,b8]);
let o3 = new Order([b3,b6]);
let o4 = new Order([b4,b7]);
let o5 = new Order([b5,b7,b8]);

let c1 = new Customer([o1,o2,o3]);
let c2 = new Customer([o4,o5]);
let c3 = new Customer([o4]);

let e1 = new Employee('Romania',[c1,c2]);
let e2 = new Employee('Romania',[c3]);
let e3 = new Employee('China',collect([c1,c3]));


let input = [e1,e2,e3];
console.log("input ->",input);

export default input;
