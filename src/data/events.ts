import collect from 'collect.js';

let data = collect( [
	{
		type: "PushEvent",
		data: "..."
	},
	{
		type: "CreateEvent",
		data: "..."
	},
	{
		type: "CreateEvent",
		data: "..."
	},
	{
		type: "PushEvent",
		data: "..."
	},
	{
		type: "IssueEvent",
		data: "..."
	},
	{
		type: "OtherEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
	{
		type: "PushEvent",
		data: "..."
	},
	{
		type: "OtherEvent",
		data: "..."
	},
	{
		type: "IssueEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
	{
		type: "PushEvent",
		data: "..."
	},
	{
		type: "OtherEvent",
		data: "..."
	},
	{
		type: "IssueEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
	{
		type: "CommentEvent",
		data: "..."
	},
] );


console.log( "input ->", data);
export default data;