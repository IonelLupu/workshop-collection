import employees from './data/employees'

const minSales = 100;
export default (function getTotalRevenueForCountry ( country ) {

	let totalRevenue = 0;

	for ( let employee of employees ) {
		let employeeCountry = employee.country;
		if ( employeeCountry === country ) {
			let customers = employee.customers;
			for ( let customer of customers ) {
				let customerTotalSales = 0;
				for ( let order of customer.orders ) {
					customerTotalSales += order.total;
				}

				if(customerTotalSales > minSales){
					totalRevenue += customerTotalSales;
				}
			}
		}
	}

	console.log( "Total revenue->", totalRevenue );
})('Romania')